<?php

namespace App\Models\Auth\Traits\Relationship;

use App\Models\Auth\Region;
use App\Models\Auth\Address;
use App\Models\Auth\UserDetail;
use App\Models\Auth\SocialAccount;
use App\Models\Auth\PasswordHistory;

/**
 * Class UserRelationship.
 */
trait UserRelationship
{
    /**
     * @return mixed
     */
    public function providers()
    {
        return $this->hasMany(SocialAccount::class);
    }

    /**
     * @return mixed
     */
    public function passwordHistories()
    {
        return $this->hasMany(PasswordHistory::class);
    }

    public function address()
    {
        return $this->hasMany(Address::class);
    }

    /**
     *  @return mixed
     */
    public function region()
    {
        return $this->hasOne(Region::class);
    }
}
