<?php

namespace App\Models\Auth;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    public $fillable = ['address'];
}
