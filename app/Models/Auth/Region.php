<?php

namespace App\Models\Auth;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    public $fillabel = ['region_code', 'region_name'];
}
